
Affluent
Developer Test Task
OVERVIEW
The following document includes instructions for a development task we use to measure your skill level.
What we’re looking for:
The instructions are vague - there is no “right” solution.
Keep it simple! This test is designed to take you no more than a few hours to complete.
Ask questions. Good communication is the key to our team. You should be able to complete this task without help, but if you have questions about expectations - ask.
TL;DR
Create a new Node.js project and push it into a git repo.
Create a MySQL database.
Create a Node.js script that will fetch data from a REST API and store it in the database you created
Create a Node.js script that will fetch data from a website using puppeteer and store it in the database.
DETAILS
1 - Create a new Node.js project and push it into a git repo 
Create a new repository on any git hosting platform of your choice.
2 - Create a MySQL database 
Feel free to use whatever structure you think fits the tasks. You can use docker-compose or some free online MySQL hosting solution:
https://www.freemysqlhosting.net
https://www.db4free.net
https://www.freesqldatabase.com/freemysqldatabase
3 - Create a Node.js script that will fetch data from an API
Create a simple script that will fetch data from an API and store it in the database you created.
Use https://reqres.in/ for the API.
Fetch the list of users from that API (all pages).
Store the fetched data in the database.
4 - Create a Node.js script to fetch data from a website using puppeteer 
Create a simple script that uses a puppeteer to fetch data from https://develop.pub.afflu.net.
Log into account using these credentials:
Login: developertest@affluent.io
Password: KNP8105QX2LZ44KKTIryv5oyhfQKdC
Navigate to https://develop.pub.afflu.net/list?type=dates
Change the date range to October 1st, 2020 => October 30th, 2020
Fetch the whole “DATES” table at the bottom of the page (all pages).
Store data from the table in the database.
