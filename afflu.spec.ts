// import type {
//   // HTTPRequest,
//   HTTPResponse
// } from "puppeteer"

export {}

const base = "https://develop.pub.afflu.net"
, toGrab = "https://develop.pub.afflu.net/list?type=dates"
, api = "https://develop.pub.afflu.net/api/query/dates"
,  login = {
  "username": "developertest@affluent.io",
  "password": "KNP8105QX2LZ44KKTIryv5oyhfQKdC"
}
, loginSelector = "form.login-form"
, loginSubmit = `${loginSelector} [type=submit]`
, range = {
  "daterangepicker_start": "10/01/2020",
  "daterangepicker_end": "10/30/2020"
}


describe("login", () => {
  it("goto", () => page.goto(base))
  it("credentials", () => expect(page).toFillForm(loginSelector, login))
  it("submit", async () => {
    await expect(page).toClick(loginSubmit)
    await page.waitForNavigation()
  })
})

describe("grab", () => {  
  it("goto", () => page.goto(toGrab))
  it("modal omit", async () => {
    //TODO wtf
    await page.waitForTimeout(5000)
    await expect(page).toClick("#pushActionRefuse", {timeout: 30000})
  })
  it("date pick: open", async () => {
    // await page.waitForSelector("#datepicker")
    await expect(page).toClick("#datepicker")
  })
  it("date pick: fill", async () => {
    for (const name in range) {
      await expect(page).toFill(
        `.daterangepicker [name="${name}"]`,
        range[name as keyof typeof range]
      )
    }
  })
  it("date pick: apply and grab", async () => {
    
    // page.on("request", async (intercepted: HTTPRequest) => {
    //   const url = intercepted.url()
    //   , method = intercepted.method()
    //   if ((method === "POST" && url === api)) {
    //     console.log("req", Object.keys(await intercepted.response()?.json() ?? {}))
    //   }

    //   // intercepted.continue()
    // })
    // page.on("response", async (intercepted: HTTPResponse) => {
    //   // console.log("response")
    //   const url = intercepted.url()
    //   , method = intercepted.request().method()
    //   if (method === "POST" && url === api) {
    //     //'draw', 'data', 'recordsTotal', 'recordsFiltered' 
    //     console.log("res", url, Object.keys((await intercepted.json()).data.length))

    //   }
    //   // intercepted.res
    //   intercepted.request().continue()
    // })

    await expect(page).toClick(".applyBtn")
    await page.waitForNavigation()
    await page.$eval("#launcher", (el: Element) => el.parentElement?.removeChild(el))

    await expect(page).toClick("#DataTables_Table_0_length button")
    await page.waitForSelector("#DataTables_Table_0_length ul", {visible: true})
    // await jestPuppeteer.debug()

    
    // await page.setRequestInterception(true)
    await expect(page).toClick("#DataTables_Table_0_length li:last-child")
    // await page.waitForTimeout(1000000)
    await page.waitForResponse(api)
    expect(await page.$x("//[@class=filter-option]/text()")).toBe("All")
    // await page.setRequestInterception(false)
    

    await jestPuppeteer.debug()
    
  })
  it("wait", () => page.waitForTimeout(10000))
})
