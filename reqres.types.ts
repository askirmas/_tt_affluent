export namespace ReqRes {
  export type Users = {
    page: number
    total_pages: number
    data: Array<{
      id: number
      email: string
      first_name: string
      last_name: string
      avatar: string
    }>
  }
}