module.exports = {
  launch: {
    args: ["--wait-for-browser"],
    dumpio: false,
    headless: process.env.HEADLESS !== "0",
    waitFor: ["domcontentloaded", "load"]
  }
}