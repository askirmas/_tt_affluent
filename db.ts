import {DB_DATABASE, DB_DIALECT, DB_HOST, DB_USERNAME} from "./config"
import {Sequelize, DataTypes} from "sequelize"

const sequelize = new Sequelize({
  "host": DB_HOST,
  "dialect": DB_DIALECT,
  "username": DB_USERNAME,
  "database": DB_DATABASE
})
, {
  STRING, INTEGER
} = DataTypes
, {
  //@ts-expect-error
  UUIDV4: ID
} = Sequelize

export default sequelize

export {
  start, stop,
  ID, STRING, INTEGER
}

async function start() {
  await sequelize.authenticate()
  return sequelize.sync()
}

function stop() {
  return sequelize.close()
}