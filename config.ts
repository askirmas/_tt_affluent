import dotenv from "dotenv-extended"

const config = dotenv.load() as {
  DB_DIALECT: "mysql"|"mariadb"|"postgres"
  DB_HOST: string
  DB_USERNAME: string
  DB_DATABASE: string
} 

export = config