import Ajv from "ajv"
import formats from "ajv-formats"

const ajv = formats(new Ajv({
  "strict": true,
  "verbose": true,
  "validateSchema": false,
  "strictSchema": false
}))

export default ajv