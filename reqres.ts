#!/usr/bin/env ts-node-script

import {deepStrictEqual} from "assert"
import { URLSearchParams } from "url"
import fetch from "node-fetch"
import schema from "./reqres.schema.json"
import type { ReqRes } from "./reqres.types"
import db, {start, stop} from "./db"
import ajv from "./ajv"
import UsersAttributes, { UserModel } from "./reqres.model"

const {$id, base} = schema
, ReqResUsers = db.define<UserModel>("ReqResUsers", UsersAttributes)

ajv.addSchema(schema)

check()

async function check() {
  try {
    await start()
    deepStrictEqual(await main(), {pointer: 3, total: 2})
  } catch (e) {
    throw e
  }

  await stop()
}

async function main() {
  let total: undefined|number
  , pointer: undefined|number

  do {
    const res: ReqRes.Users = await getUsers(total)

    if (!ajv.validate($id, res)) 
      throw [
        `${pointer}/${total}`,
        ajv.errorsText(ajv.errors)
      ].join("\n")

    const {
      data,
      page,
      total_pages
    } = res

    await ReqResUsers.bulkCreate(data, {updateOnDuplicate: ["id"]})

    total = total_pages
    pointer = page + 1
  } while(pointer! <= total!)

  return {pointer, total}
}

async function getUsers(page: undefined | ReqRes.Users["page"]) {
  return await (await fetch(`${
    base
  }?${
    new URLSearchParams({
      page: page === undefined ? undefined : `${page}`
    })
  }`)).json()
}
