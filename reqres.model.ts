import type { AbstractDataType, Model } from "sequelize"
import type { ModelAttributes, NumberDataTypeConstructor, StringDataTypeConstructor } from "sequelize/types"
import {STRING, INTEGER} from "./db"

// https://sequelize.org/master/manual/typescript.html

type SeqTypeToTs<T> = T extends NumberDataTypeConstructor ? number 
: T extends StringDataTypeConstructor ? string
: AbstractDataType

type SeqToTypes<A extends ModelAttributes> = {[k in keyof A]:
  "type" extends keyof A[k]
  ? SeqTypeToTs<A[k]["type"]>
  :SeqTypeToTs<A[k]>
}

const UsersAttributes = {
  "id": {
    "type": INTEGER,
    "primaryKey": true,
    "allowNull": false
  },
  "email": {
    "type": STRING,
    "allowNull": false
  },
  "first_name": {
    "type": STRING,
    "allowNull": false
  },
  "last_name": {
    "type": STRING,
    "allowNull": false
  },
  "avatar": {
    "type": STRING,
    "allowNull": false
  }
} as const

type tUserAttr = SeqToTypes<typeof UsersAttributes>

export type UserModel = Model<tUserAttr, tUserAttr>
export default UsersAttributes